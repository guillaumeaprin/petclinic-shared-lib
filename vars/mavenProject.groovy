def call (String teste){

        pipeline {

        agent any
        environment {
            ACCESS_KEY = credentials('urlteams')
        }
        stages {
                 
            stage('verify') {
                steps {
                    sh './mvnw -v'
                    echo 'verify'
                }
            }
            stage('compile') {
                steps {
                sh  './mvnw clean compile'
                }
            }
            stage('test') {
                steps {
                    sh './mvnw test'
                }
            }
            stage('quality') {
                steps {
                    withSonarQubeEnv('sonarcube') {
                        sh './mvnw sonar:sonar -Pcoverage'
                    }
                }
            }  
        }  
        post {
            always {
                junit 'target/surefire-reports/*.xml'
            }
            success {
               // office365ConnectorSend message: 'Guillaume', status: 'positif', webhookUrl: 'https://oddet.webhook.office.com/webhookb2/b6173027-8848-430e-89bf-e0b8ead5430c@75727609-df27-489a-9c9d-495dacd487cd/JenkinsCI/0c6f24f998f04563a229e622d5f78a26/d47dc24d-2fc7-46a5-8712-ec2644617056'
            }
            failure {
    //office365ConnectorSend message: 'Guillaume', status: 'negatif', webhookUrl: 'https://oddet.webhook.office.com/webhookb2/b6173027-8848-430e-89bf-e0b8ead5430c@75727609-df27-489a-9c9d-495dacd487cd/JenkinsCI/0c6f24f998f04563a229e622d5f78a26/d47dc24d-2fc7-46a5-8712-ec2644617056'
            }

        }

    }
   
    


}


